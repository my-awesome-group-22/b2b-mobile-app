class PatientsModel {
  final String profilePic;
  final String patientName;
  final String patientLastAppointment;

  PatientsModel({
    required this.profilePic,
    required this.patientLastAppointment,
    required this.patientName,
  });
}

List<PatientsModel> patients = [
  PatientsModel(
    profilePic: "assets/img/profile2.png",
    patientLastAppointment: "1h ago",
    patientName: "Jason LeBron",
  ),
  PatientsModel(
    profilePic: "assets/img/profile3.png",
    patientLastAppointment: "2 days ago",
    patientName: "Bessie Cooper",
  ),
  PatientsModel(
    profilePic: "assets/img/account.png",
    patientLastAppointment: "2 days ago",
    patientName: "Floyd Miles",
  ),
];
