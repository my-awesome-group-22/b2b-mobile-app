class ChatsModel {
  final String profilePic;
  final String name;
  final String lastMessage;
  final bool isOnline;

  ChatsModel({
    required this.profilePic,
    required this.name,
    required this.lastMessage,
    required this.isOnline,
  });
}

List<ChatsModel> chats = [
  ChatsModel(
    profilePic: "assets/img/profile1.png",
    name: 'Jane Cooper',
    lastMessage: 'Thanks',
    isOnline: false,
  ),
  ChatsModel(
    profilePic: "assets/img/profile2.png",
    name: 'Jason LeBron',
    lastMessage: 'Next Time',
    isOnline: true,
  ),
  ChatsModel(
    profilePic: "assets/img/account.png",
    name: 'Floyd Miles',
    lastMessage: 'Next Time',
    isOnline: true,
  ),
  ChatsModel(
    profilePic: "assets/img/profile3.png",
    name: 'Bessie Cooper',
    lastMessage: 'Thanks',
    isOnline: false,
  ),
];
