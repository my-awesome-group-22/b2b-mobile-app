import 'package:b2b/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FeatureTiles extends StatelessWidget {
  final String text;
  const FeatureTiles({Key? key, required this.text,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: ScreenUtil().setHeight(10),
          width: ScreenUtil().setHeight(10),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: AppColors.secondary,
            boxShadow: [
              BoxShadow(
                color: AppColors.secondary.withOpacity(0.3),
                spreadRadius: 5,
                blurRadius: 1,
              ),
            ],
          ),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(15),
        ),
        Flexible(
          child: Text(
            text,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(15),
              fontWeight: FontWeight.w400,
            ),
            maxLines: 3,
          ),
        ),
      ],
    );
  }
}
