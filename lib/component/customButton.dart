import 'package:flutter/material.dart';

import '../styles.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final IconData? icon;
  final VoidCallback onPressed;

  const CustomButton({
    Key? key,
    required this.title,
    this.icon,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      borderRadius: BorderRadius.circular(80),
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.accentLight.withOpacity(0.1),
          borderRadius: BorderRadius.circular(80),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 10,
          ),
          child: Row(
            children: [
              icon!= null ? Icon(
                icon,
                color: AppColors.primary,
                size: Theme.of(context)
                    .textTheme
                    .titleLarge!
                    .fontSize,
              ) : Container(),
              const SizedBox(
                width: 10,
              ),
              Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(
                  color: AppColors.primary,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
