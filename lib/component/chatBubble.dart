import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../styles.dart';

class ChatBubble extends StatelessWidget {
  final Widget content;
  final Color? color;

  const ChatBubble({
    Key? key,
    required this.content,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth / 1.2,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: color ?? AppColors.accentLight.withOpacity(0.2),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: content,
      ),
    );
  }
}
