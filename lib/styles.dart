import 'package:flutter/material.dart';

class AppColors {
  static Color primary = const Color(0xFF0C1E3C);
  static Color primaryLight = const Color(0xFF041E42);

  static Color secondary = const Color(0xFF21355C);
  static Color secondaryLight = const Color(0xFF1B365D);

  static Color accent = const Color(0xFF3683FC);
  static Color accentLight = const Color(0xFF307FE2);
  static const Gradient buttonGradientColor = LinearGradient(
    tileMode: TileMode.clamp,
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      Color(0xFF3683FC),
      Color(0xFF21355C),
    ],
  );
}