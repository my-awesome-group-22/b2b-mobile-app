import 'package:b2b/screens/helloPage.dart';
import 'package:b2b/screens/profilePicture.dart';
import 'package:b2b/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:line_icons/line_icons.dart';

class AuthenticationView extends StatefulWidget {
  const AuthenticationView({Key? key}) : super(key: key);

  @override
  State<AuthenticationView> createState() => _AuthenticationViewState();
}

class _AuthenticationViewState extends State<AuthenticationView> {
  late String language = "EN", email, password, profilePicture;
  int pageIndex = 0;
  late PageController _pageController;
  bool hidePass = true;
  List<int> passwordStrength = [];
  final emailKey = GlobalKey<FormState>();

  @override
  void initState() {
    _pageController = PageController(
      initialPage: pageIndex,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: false,
        backgroundColor: Colors.transparent,
        leading: pageIndex != 0
            ? IconButton(
                icon: Icon(
                  LineIcons.angleLeft,
                  color: AppColors.secondary,
                  size: 30,
                ),
                onPressed: () {
                  _pageController.previousPage(
                    duration: const Duration(milliseconds: 5),
                    curve: Curves.linear,
                  );
                },
              )
            : Container(),
        title: pageIndex == 1
            ? Text(
                "Change email",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  color: AppColors.secondary,
                ),
              )
            : Container(),
        actions: [
          pageIndex == 0
              ? InkWell(
                  onTap: () async {
                    String lan = await showModalBottomSheet(
                      context: context,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30),
                        ),
                      ),
                      builder: (context) {
                        return StatefulBuilder(
                          builder: (context, setState) {
                            return Wrap(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 30,
                                  ),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        children: [
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          Container(
                                            width: 150,
                                            height: 5,
                                            color: const Color(0xFFE7E9EC),
                                          ),
                                          const SizedBox(
                                            height: 30,
                                          ),
                                          Text(
                                            "Select Language",
                                            style: TextStyle(
                                              fontSize: 25,
                                              fontWeight: FontWeight.w400,
                                              color: AppColors.secondary,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: TextFormField(
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                  borderSide: const BorderSide(
                                                    color: Color(0xFFE7E9EC),
                                                    width: 0.1,
                                                  ),
                                                ),
                                                enabledBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                  borderSide: const BorderSide(
                                                    color: Color(0xFFE7E9EC),
                                                    width: 0.1,
                                                  ),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                  borderSide: const BorderSide(
                                                    color: Color(0xFFE7E9EC),
                                                    width: 0.1,
                                                  ),
                                                ),
                                                fillColor:
                                                    const Color(0xFFE7E9EC),
                                                filled: true,
                                                hintText: "Search Language",
                                                hintStyle: const TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xFF909BA6),
                                                ),
                                                suffixIcon: Icon(
                                                  Icons.search,
                                                  size: 25,
                                                  color: AppColors.secondary,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(30.0),
                                            child: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Image.asset(
                                                          "assets/img/US.png",
                                                        ),
                                                        const SizedBox(
                                                          width: 20,
                                                        ),
                                                        Text(
                                                          "English (US)",
                                                          style: TextStyle(
                                                            fontSize: 20,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: AppColors
                                                                .secondary,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Radio(
                                                      value: "EN",
                                                      groupValue: language,
                                                      onChanged: (value) {
                                                        setState(() {
                                                          language =
                                                              value.toString();
                                                        });
                                                      },
                                                    ),
                                                  ],
                                                ),
                                                const SizedBox(
                                                  height: 20,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Image.asset(
                                                          "assets/img/RU.png",
                                                        ),
                                                        const SizedBox(
                                                          width: 20,
                                                        ),
                                                        Text(
                                                          "Russian",
                                                          style: TextStyle(
                                                            fontSize: 20,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: AppColors
                                                                .secondary,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Radio(
                                                      value: "RU",
                                                      groupValue: language,
                                                      onChanged: (value) {
                                                        setState(() {
                                                          language =
                                                              value.toString();
                                                        });
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              Navigator.pop(context, language);
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                color: AppColors.accent,
                                              ),
                                              child: const Padding(
                                                padding: EdgeInsets.symmetric(
                                                  horizontal: 50,
                                                  vertical: 12,
                                                ),
                                                child: Text(
                                                  "Done",
                                                  style: TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            );
                          },
                        );
                      },
                    );
                    if (lan != "" || lan.isNotEmpty) {
                      setState(() {
                        language = lan;
                      });
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                      right: 20,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          language,
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w400,
                            color: AppColors.secondary,
                          ),
                        ),
                        Icon(
                          Icons.expand_more,
                          color: AppColors.secondary,
                          size: 40,
                        ),
                      ],
                    ),
                  ),
                )
              : Container(),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: PageView(
          pageSnapping: true,
          physics: const NeverScrollableScrollPhysics(),
          controller: _pageController,
          onPageChanged: (value) {
            setState(() {
              pageIndex = value;
            });
          },
          /*
          * First page view widget is the email page where auth is done on whether the email exists and if it does
          * it will navigate to page 3 else page 2 for account creation.
          * (yet to be implemented)
          * */
          children: [
            //email page
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/img/hand.png",
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Hello there",
                          style: TextStyle(
                            color: AppColors.accent,
                            fontSize: 35,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          "Let's get in contact",
                          style: TextStyle(
                            color: AppColors.secondary,
                            fontSize: 35,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Login or create your account using your email or phone number",
                      style: TextStyle(
                        fontSize: 18,
                        color: AppColors.secondary,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      key: emailKey,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        hintText: "email or phone",
                        hintStyle: TextStyle(
                          fontSize: 30,
                          color: AppColors.accentLight.withOpacity(0.4),
                        ),
                      ),
                      onChanged: (value) {
                        if (value.isNotEmpty) {
                          setState(() {
                            email = value;
                          });
                        }
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Email or Phone is Missing";
                        } else {
                          setState(() {
                            email = value;
                          });
                          return null;
                        }
                      },
                      style: TextStyle(
                        fontSize: 30,
                        color: AppColors.secondary,
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {},
                          child: Image.asset(
                            "assets/img/facebookLogo.png",
                          ),
                        ),
                        const SizedBox(
                          width: 50,
                        ),
                        InkWell(
                          onTap: () {},
                          child: Image.asset(
                            "assets/img/googleLogo.png",
                          ),
                        ),
                        const SizedBox(
                          width: 50,
                        ),
                        InkWell(
                          onTap: () {},
                          child: Image.asset(
                            "assets/img/appleLogo.png",
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        _pageController.nextPage(
                          duration: const Duration(milliseconds: 5),
                          curve: Curves.linear,
                        );
                        setState(() {
                          pageIndex++;
                        });
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: AppColors.accent,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Center(
                            child: Text(
                              "Continue",
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ],
            ),
            //verification page
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/img/mail.png",
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    RichText(
                      text: TextSpan(
                          text: "Let me ",
                          style: TextStyle(
                            color: AppColors.secondary,
                            fontSize: 35,
                            fontWeight: FontWeight.w400,
                          ),
                          children: [
                            TextSpan(
                              text: "verify ",
                              style: TextStyle(
                                color: AppColors.accent,
                                fontSize: 35,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            TextSpan(
                              text: "your\nidentity...",
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 35,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ]),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "We have sent you a message with 6-digit code, or use the code from your Authenticator App.",
                      style: TextStyle(
                        fontSize: 18,
                        color: AppColors.secondary,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                      ),
                      onChanged: (value) {
                        if (value.isNotEmpty) {
                          setState(() {
                            password = value;
                          });
                        }
                      },
                      style: TextStyle(
                        fontSize: 30,
                        color: AppColors.secondary,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          LineIcons.phone,
                          color: AppColors.accent,
                          size: 25,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Call Me",
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: AppColors.accent,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: Column(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Text(
                              "send it again",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5!
                                  .copyWith(
                                    color: AppColors.accent,
                                  ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Be quick, code expires in 6 hours!",
                            style:
                                Theme.of(context).textTheme.caption!.copyWith(
                                      color: AppColors.secondary,
                                      fontSize: 15,
                                    ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        _pageController.nextPage(
                          duration: const Duration(milliseconds: 5),
                          curve: Curves.linear,
                        );
                        setState(() {
                          pageIndex++;
                        });
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: AppColors.accent,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Center(
                            child: Text(
                              "Continue",
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ],
            ),
            //name page
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/img/account.png",
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    RichText(
                      text: TextSpan(
                          text: "What name do you\nprefer to be ",
                          style: TextStyle(
                            color: AppColors.secondary,
                            fontSize: 30,
                            fontWeight: FontWeight.w400,
                          ),
                          children: [
                            TextSpan(
                              text: "called",
                              style: TextStyle(
                                color: AppColors.accent,
                                fontSize: 30,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            TextSpan(
                              text: "?",
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 30,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ]),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        hintText: "Jane Cooper",
                        hintStyle: TextStyle(
                          fontSize: 30,
                          color: AppColors.accentLight.withOpacity(0.4),
                        ),
                      ),
                      onChanged: (value) {
                        if (value.isNotEmpty) {
                          setState(() {
                            password = value;
                          });
                        }
                      },
                      style: TextStyle(
                        fontSize: 30,
                        color: AppColors.secondary,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () async {
                            String profile = await Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => const ProfilePicture(),
                              ),
                            );
                            if (profile.isNotEmpty || profile != "") {
                              setState(() {
                                profilePicture = profile;
                              });
                            }
                          },
                          child: RichText(
                            text: TextSpan(
                              text: "Add ",
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                              ),
                              children: [
                                TextSpan(
                                  text: "Profile Picture",
                                  style: TextStyle(
                                    color: AppColors.accent,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {},
                      child: Text(
                        "Ask me later",
                        style: Theme.of(context).textTheme.titleLarge!.copyWith(
                              color: AppColors.accentLight,
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        _pageController.nextPage(
                          duration: const Duration(milliseconds: 5),
                          curve: Curves.linear,
                        );
                        setState(() {
                          pageIndex++;
                        });
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: AppColors.accent,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Center(
                            child: Text(
                              "Continue",
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ],
            ),
            //password page(create account)
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/img/lock.png",
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    RichText(
                      text: TextSpan(
                          text: "Now your ",
                          style: TextStyle(
                            color: AppColors.secondary,
                            fontSize: 35,
                            fontWeight: FontWeight.w400,
                          ),
                          children: [
                            TextSpan(
                              text: "password",
                              style: TextStyle(
                                color: AppColors.accent,
                                fontSize: 35,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ]),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Avoid sharing it with anyone!",
                      style: TextStyle(
                        fontSize: 18,
                        color: AppColors.secondary,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        hintText: "************",
                        hintStyle: TextStyle(
                          fontSize: 30,
                          color: AppColors.accentLight.withOpacity(0.4),
                        ),
                        suffixIcon: IconButton(
                          splashRadius: 10,
                          onPressed: () {
                            setState(() {
                              hidePass = !hidePass;
                            });
                          },
                          icon: Icon(
                            hidePass
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                            color: hidePass
                                ? AppColors.secondary.withOpacity(0.5)
                                : AppColors.secondary,
                            size: 30,
                          ),
                        ),
                      ),
                      onChanged: (value) {
                        if (value.isNotEmpty) {
                          setState(() {
                            password = value;
                          });
                        }
                      },
                      obscureText: hidePass,
                      style: TextStyle(
                        fontSize: 30,
                        color: AppColors.secondary,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          height: 5,
                          child: ListView.separated(
                            shrinkWrap: true,
                            itemCount: 4,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return Container(
                                width: ScreenUtil().setWidth(40),
                                height: 1,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: const Color(0xFFF0C105),
                                ),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return const SizedBox(
                                width: 15,
                              );
                            },
                          ),
                        ),
                        const Text(
                          "Strong",
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFFF0C105),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          useSafeArea: true,
                          barrierColor: AppColors.accent.withOpacity(0.2),
                          builder: (context) {
                            return Dialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Container(
                                padding: const EdgeInsets.only(
                                  left: 30,
                                  right: 10,
                                  top: 30,
                                  bottom: 30,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "Creating a Password",
                                      style: TextStyle(
                                        color: AppColors.secondary,
                                        fontSize: 25,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Text(
                                      "Create an strong password to protect your data",
                                      style: TextStyle(
                                        color: AppColors.secondary
                                            .withOpacity(0.8),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "Requirements",
                                          style: TextStyle(
                                            color: AppColors.secondary,
                                            fontSize: 23,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    ListTile(
                                      horizontalTitleGap: 15,
                                      minLeadingWidth: 10,
                                      leading: Container(
                                        width: 6,
                                        height: 6,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppColors.secondary,
                                        ),
                                      ),
                                      title: Text(
                                        "8 or more characters and numbers",
                                        style: TextStyle(
                                          color: AppColors.secondary
                                              .withOpacity(0.8),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                    ListTile(
                                      horizontalTitleGap: 15,
                                      minLeadingWidth: 10,
                                      leading: Container(
                                        width: 6,
                                        height: 6,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppColors.secondary,
                                        ),
                                      ),
                                      title: Text(
                                        "At least 1 special character [{(!@#\$%&*_-=+?/;.,)}]",
                                        style: TextStyle(
                                          color: AppColors.secondary
                                              .withOpacity(0.8),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                    ListTile(
                                      horizontalTitleGap: 15,
                                      minLeadingWidth: 10,
                                      dense: true,
                                      leading: Container(
                                        width: 6,
                                        height: 6,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppColors.secondary,
                                        ),
                                      ),
                                      title: Transform.translate(
                                        offset: const Offset(0, -5),
                                        child: Text(
                                          "Avoid dates and sequences",
                                          style: TextStyle(
                                            color: AppColors.secondary
                                                .withOpacity(0.8),
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      },
                      child: RichText(
                        text: TextSpan(
                            text: "Check ",
                            style: TextStyle(
                              color: AppColors.secondary,
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                            ),
                            children: [
                              TextSpan(
                                text: "Password Tips",
                                style: TextStyle(
                                  color: AppColors.accent,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ]),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        _pageController.nextPage(
                          duration: const Duration(milliseconds: 5),
                          curve: Curves.linear,
                        );
                        setState(() {
                          pageIndex++;
                        });
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: AppColors.accent,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Center(
                            child: Text(
                              "Continue",
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ],
            ),
            //password page(login)
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/img/pass.png",
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    RichText(
                      text: TextSpan(
                          text: "Now your ",
                          style: TextStyle(
                            color: AppColors.secondary,
                            fontSize: 35,
                            fontWeight: FontWeight.w400,
                          ),
                          children: [
                            TextSpan(
                              text: "password",
                              style: TextStyle(
                                color: AppColors.accent,
                                fontSize: 35,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ]),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Avoid sharing it with anyone!",
                      style: TextStyle(
                        fontSize: 18,
                        color: AppColors.secondary,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.accentLight.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(0.5),
                            width: 1,
                          ),
                        ),
                        hintText: "************",
                        hintStyle: TextStyle(
                          fontSize: 30,
                          color: AppColors.accentLight.withOpacity(0.4),
                        ),
                        suffixIcon: IconButton(
                          splashRadius: 10,
                          onPressed: () {
                            setState(() {
                              hidePass = !hidePass;
                            });
                          },
                          icon: Icon(
                            hidePass
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                            color: hidePass
                                ? AppColors.secondary.withOpacity(0.5)
                                : AppColors.secondary,
                            size: 30,
                          ),
                        ),
                      ),
                      onChanged: (value) {
                        if (value.isNotEmpty) {
                          setState(() {
                            password = value;
                          });
                        }
                      },
                      obscureText: hidePass,
                      style: TextStyle(
                        fontSize: 30,
                        color: AppColors.secondary,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "Forgotten Password?",
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: AppColors.secondary,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) => const HelloPage(),
                          ),
                        );
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: AppColors.accent,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Center(
                            child: Text(
                              "Done",
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
