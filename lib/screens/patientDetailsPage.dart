import 'package:b2b/component/chatBubble.dart';
import 'package:b2b/component/customButton.dart';
import 'package:b2b/screens/biomarkerEditorPage.dart';
import 'package:b2b/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:line_icons/line_icons.dart';

class PatientDetailsPage extends StatefulWidget {
  const PatientDetailsPage({Key? key}) : super(key: key);

  @override
  State<PatientDetailsPage> createState() => _PatientDetailsPageState();
}

class _PatientDetailsPageState extends State<PatientDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.chevron_left,
            color: AppColors.primary,
            size: Theme.of(context).textTheme.headline3!.fontSize,
          ),
        ),
        title: Text(
          "Patient details",
          style: Theme.of(context).textTheme.headline5!.copyWith(
                color: AppColors.primary,
                fontWeight: FontWeight.w500,
              ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: 10,
        ),
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppColors.accent.withOpacity(0.1),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 15,
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "By default we create an Disease tracker account for each patient, allowing them to benefit from our ecosystem",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: AppColors.primary,
                              fontWeight: FontWeight.w400,
                              fontSize: ScreenUtil().setSp(15),
                            ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () {},
                            child: Text(
                              "Show Credential",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: AppColors.accent,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomButton(
                      title: "Archive",
                      icon: LineIcons.archive,
                      onPressed: () {},
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    CustomButton(
                      title: "Sync",
                      icon: LineIcons.syncIcon,
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return SimpleDialog(
                              title: Center(
                                child: Text(
                                  "Syncing",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5!
                                      .copyWith(
                                        color: AppColors.primary,
                                        fontWeight: FontWeight.w600,
                                      ),
                                ),
                              ),
                              children: [
                                Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 20,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Some biomarkers where you update by both parties",
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium!
                                                .copyWith(
                                                  color: AppColors.primary,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            "Tap to select items to keep",
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium!
                                                .copyWith(
                                                  color: AppColors.primary,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: DataTable(
                                        headingRowColor:
                                            MaterialStateColor.resolveWith(
                                          (states) => AppColors.accentLight
                                              .withOpacity(0.2),
                                        ),
                                        columnSpacing: 20,
                                        horizontalMargin: 10,
                                        border: TableBorder.all(
                                          width: 5.0,
                                          color: Colors.white,
                                        ),
                                        columns: [
                                          DataColumn(
                                            label: Text(
                                              "Biomarkers",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                    color: AppColors.primary,
                                                  ),
                                            ),
                                          ),
                                          DataColumn(
                                            label: Text(
                                              "old value",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                    color: AppColors.primary,
                                                  ),
                                            ),
                                          ),
                                          DataColumn(
                                            label: Text(
                                              "new value",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                    color: AppColors.primary,
                                                  ),
                                            ),
                                          ),
                                          DataColumn(
                                            label: Text(
                                              "-",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1!
                                                  .copyWith(
                                                    color: AppColors.primary,
                                                  ),
                                            ),
                                          ),
                                          DataColumn(
                                            label: Icon(
                                              Icons.delete_outline,
                                              color: Colors.redAccent,
                                              size: Theme.of(context)
                                                  .textTheme
                                                  .headline5!
                                                  .fontSize,
                                            ),
                                          ),
                                          DataColumn(
                                            label: Icon(
                                              Icons.info_outline,
                                              color: AppColors.primary,
                                              size: Theme.of(context)
                                                  .textTheme
                                                  .headline5!
                                                  .fontSize,
                                            ),
                                          ),
                                        ],
                                        rows: [
                                          DataRow(
                                            color: MaterialStateProperty.all(
                                              AppColors.accentLight
                                                  .withOpacity(0.2),
                                            ),
                                            cells: [
                                              DataCell(
                                                Text(
                                                  "Blood type",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "A+",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "-",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "-",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Icon(
                                                  Icons.delete_outline,
                                                  color: Colors.redAccent,
                                                  size: Theme.of(context)
                                                      .textTheme
                                                      .headline5!
                                                      .fontSize,
                                                ),
                                              ),
                                              DataCell(
                                                Icon(
                                                  Icons.info_outline,
                                                  color: AppColors.primary,
                                                  size: Theme.of(context)
                                                      .textTheme
                                                      .headline5!
                                                      .fontSize,
                                                ),
                                              ),
                                            ],
                                          ),
                                          DataRow(
                                            color: MaterialStateProperty.all(
                                              AppColors.accentLight
                                                  .withOpacity(0.2),
                                            ),
                                            cells: [
                                              DataCell(
                                                Text(
                                                  "Blood RhD",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "positive",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "-",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "-",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Icon(
                                                  Icons.delete_outline,
                                                  color: Colors.redAccent,
                                                  size: Theme.of(context)
                                                      .textTheme
                                                      .headline5!
                                                      .fontSize,
                                                ),
                                              ),
                                              DataCell(
                                                Icon(
                                                  Icons.info_outline,
                                                  color: AppColors.primary,
                                                  size: Theme.of(context)
                                                      .textTheme
                                                      .headline5!
                                                      .fontSize,
                                                ),
                                              ),
                                            ],
                                          ),
                                          DataRow(
                                            color: MaterialStateProperty.all(
                                              Colors.red.withOpacity(0.2),
                                            ),
                                            cells: [
                                              DataCell(
                                                Text(
                                                  "parameter n..me",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "value",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "unit",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  "unit",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                      ),
                                                ),
                                              ),
                                              DataCell(
                                                Icon(
                                                  Icons.delete_forever_outlined,
                                                  color: AppColors.primary,
                                                  size: Theme.of(context)
                                                      .textTheme
                                                      .headline5!
                                                      .fontSize,
                                                ),
                                              ),
                                              DataCell(
                                                Icon(
                                                  Icons.info_outline,
                                                  color: AppColors.primary,
                                                  size: Theme.of(context)
                                                      .textTheme
                                                      .headline5!
                                                      .fontSize,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                const BiomarkerEditorPage(),
                                          ),
                                        );
                                      },
                                      borderRadius: BorderRadius.circular(50),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: AppColors.accent,
                                          borderRadius:
                                              BorderRadius.circular(50),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 25,
                                            vertical: 10,
                                          ),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                "All Biomarkers",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleMedium!
                                                    .copyWith(
                                                      color: Colors.white,
                                                    ),
                                              ),
                                              const SizedBox(
                                                width: 10,
                                              ),
                                              Icon(
                                                Icons.north_east,
                                                color: Colors.white,
                                                size: Theme.of(context)
                                                    .textTheme
                                                    .headline6!
                                                    .fontSize,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 40,
                                    ),
                                    Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 15,
                                          ),
                                          child: Container(
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.red.withOpacity(0.2),
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                            ),
                                            child: Center(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Text(
                                                  "Cancel",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .titleMedium!
                                                      .copyWith(
                                                        color: Colors.red,
                                                      ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 15,
                                          ),
                                          child: Container(
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Colors.green.shade800
                                                  .withOpacity(0.2),
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                            ),
                                            child: Center(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Text(
                                                  "Merge",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .titleMedium!
                                                      .copyWith(
                                                        color: Colors
                                                            .green.shade900,
                                                      ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 15,
                                          ),
                                          child: Container(
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                              color: AppColors.accent
                                                  .withOpacity(0.1),
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                            ),
                                            child: Center(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Text(
                                                  "Keep latest only",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .titleMedium!
                                                      .copyWith(
                                                        color: AppColors.accent,
                                                      ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            );
                          },
                        );
                      },
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    CustomButton(
                      title: "Export Data",
                      icon: LineIcons.download,
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Column(
                children: [
                  Container(
                    width: ScreenUtil().setWidth(70),
                    height: ScreenUtil().setWidth(70),
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage(
                          "assets/img/profile1.png",
                        ),
                        fit: BoxFit.contain,
                        filterQuality: FilterQuality.high,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: [
                      Text(
                        "Jane Cooper",
                        style: Theme.of(context).textTheme.headline5!.copyWith(
                              color: AppColors.primary,
                            ),
                      ),
                      Text(
                        "jane.cooper@longevityintime.org",
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: AppColors.primary.withOpacity(0.8),
                                  fontWeight: FontWeight.w400,
                                ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          children: [
                            Icon(
                              LineIcons.birthdayCake,
                              color: AppColors.primary,
                              size: Theme.of(context)
                                  .textTheme
                                  .headline5!
                                  .fontSize,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              children: [
                                Text(
                                  "1970/01/01",
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: AppColors.primary,
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                                Text(
                                  "52 years old",
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        color: AppColors.primary,
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.expand,
                              color: AppColors.primary,
                              size: Theme.of(context)
                                  .textTheme
                                  .headline5!
                                  .fontSize,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              children: [
                                Text(
                                  "1.65m",
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: AppColors.primary,
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                                Text(
                                  "Height",
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        color: AppColors.primary,
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Row(
                          children: [
                            Icon(
                              LineIcons.dumbbell,
                              color: AppColors.primary,
                              size: Theme.of(context)
                                  .textTheme
                                  .headline5!
                                  .fontSize,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              children: [
                                Text(
                                  "70Kg",
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: AppColors.primary,
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                                Text(
                                  "Weight",
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        color: AppColors.primary,
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const BiomarkerEditorPage(),
                        ),
                      );
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.accent,
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 25,
                          vertical: 10,
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "All Biomarkers",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: Colors.white,
                                  ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Icon(
                              Icons.north_east,
                              color: Colors.white,
                              size: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .fontSize,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Messages",
                            style:
                                Theme.of(context).textTheme.headline5!.copyWith(
                                      color: AppColors.primary,
                                    ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ChatBubble(
                        content: Text(
                          "Yes, I found",
                          style:
                              Theme.of(context).textTheme.titleMedium!.copyWith(
                                    color: AppColors.primary,
                                  ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      ChatBubble(
                        content: Text(
                          "Here is the test",
                          style:
                              Theme.of(context).textTheme.titleMedium!.copyWith(
                                    color: AppColors.primary,
                                  ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      ChatBubble(
                        content: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "test.pdf",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                color: AppColors.primary,
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Wrap(
                              runSpacing: 10,
                              spacing: 5,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: AppColors.accentLight
                                        .withOpacity(0.1),
                                    borderRadius: BorderRadius.circular(80),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 10,
                                    ),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Icon(
                                          LineIcons.download,
                                          color: AppColors.primary,
                                          size: Theme.of(context)
                                              .textTheme
                                              .titleLarge!
                                              .fontSize,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          "Download",
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                            color: AppColors.primary,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    color: AppColors.accentLight
                                        .withOpacity(0.1),
                                    borderRadius: BorderRadius.circular(80),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 10,
                                    ),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Icon(
                                          LineIcons.robot,
                                          color: AppColors.primary,
                                          size: Theme.of(context)
                                              .textTheme
                                              .titleLarge!
                                              .fontSize,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          "AI Analysis",
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                            color: AppColors.primary,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    color: AppColors.accentLight
                                        .withOpacity(0.1),
                                    borderRadius: BorderRadius.circular(80),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 10,
                                    ),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Icon(
                                          LineIcons.download,
                                          color: AppColors.primary,
                                          size: Theme.of(context)
                                              .textTheme
                                              .titleLarge!
                                              .fontSize,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          "extract data",
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                            color: AppColors.primary,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        color: AppColors.accent.withOpacity(0.1),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ChatBubble(
                            content: Text(
                              "Thanks!",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                color: AppColors.primary,
                              ),
                              textAlign: TextAlign.end,
                            ),
                            color: Colors.green.shade800.withOpacity(0.2),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
      bottomSheet: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: 20,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: AppColors.accentLight.withOpacity(0.2),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15,
                    vertical: 10,
                  ),
                  child: Text(
                    "Send message",
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(
                      color: AppColors.primary,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
              width: 10,
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.send,
                color: AppColors.accent,
                size: Theme.of(context)
                    .textTheme
                    .headline5!
                    .fontSize,
              ),
            ),
          ],
        ),
      ),

    );
  }
}
