import 'package:b2b/component/featureList.dart';
import 'package:b2b/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SubscriptionPage extends StatefulWidget {
  const SubscriptionPage({Key? key}) : super(key: key);

  @override
  State<SubscriptionPage> createState() => _SubscriptionPageState();
}

class _SubscriptionPageState extends State<SubscriptionPage> {
  late PageController _controller;

  int pageIndex = 0, daysSubscription = 1, renewalSubscription = 1;
  double perMonthSubCost = 7.5,
      autoRenewalCost = 7.99,
      totalMonthly = 7.5,
      totalRenewal = 7.99;

  List<String> featureList = [
    'Full diagnosis of 20 most common diseases in seconds',
    'Full access to scientific papers library',
    'Get second opinion powered by AI',
    'Fully remote',
    'Evidence based personalized risk & recommendation reports',
  ];

  @override
  void initState() {
    _controller = PageController(
      initialPage: pageIndex,
    );
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            if (pageIndex == 0) {
            } else {
              setState(() {
                pageIndex = 0;
                _controller.previousPage(
                  duration: const Duration(milliseconds: 10),
                  curve: Curves.linear,
                );
              });
            }
          },
          icon: const Icon(
            Icons.chevron_left_sharp,
            color: Colors.black,
          ),
          iconSize: ScreenUtil().setSp(30),
        ),
      ),
      body: PageView(
        controller: _controller,
        physics: const NeverScrollableScrollPhysics(),
        pageSnapping: true,
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: ScreenUtil().setHeight(20),
                    ),
                    child: Text(
                      "Subscribe",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(35),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  Text(
                    "What you get",
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(20),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(15),
                  ),
                  ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return FeatureTiles(
                        text: featureList[index],
                      );
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        height: ScreenUtil().setHeight(15),
                      );
                    },
                    itemCount: featureList.length,
                  ),
                  Center(
                    child: TextButton(
                      onPressed: () {},
                      child: Text(
                        "All Features",
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          color: AppColors.accent,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(30),
                  ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: AppColors.buttonGradientColor,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Free trial",
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(23),
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            "Try all features for 30 days",
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(19),
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            pageIndex = 1;
                            _controller.nextPage(
                              duration: const Duration(milliseconds: 10),
                              curve: Curves.linear,
                            );
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: AppColors.accent,
                          ),
                          padding: const EdgeInsets.symmetric(
                            horizontal: 38,
                            vertical: 10,
                          ),
                          child: Text(
                            "Start Now",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: ScreenUtil().setSp(20),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 25,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 5,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '30 day subscription',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        'Pay for 2+ months in advance to get discount',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                onPressed: () {
                                  if (daysSubscription != 1) {
                                    setState(() {
                                      daysSubscription--;
                                      totalMonthly =
                                          perMonthSubCost * daysSubscription;
                                    });
                                  }
                                },
                                splashColor: Colors.transparent,
                                icon: const Icon(Icons.remove),
                              ),
                              Padding(
                                padding:
                                const EdgeInsets.symmetric(horizontal: 10),
                                child: Text(
                                  daysSubscription.toString().padLeft(2, '0'),
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(20),
                                  ),
                                ),
                              ),
                              IconButton(
                                onPressed: () {
                                  setState(() {
                                    daysSubscription++;
                                    totalMonthly =
                                        perMonthSubCost * daysSubscription;
                                  });
                                },
                                splashColor: Colors.transparent,
                                icon: const Icon(Icons.add),
                              ),
                            ],
                          ),
                          Text(
                            "USD\$ $totalMonthly",
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(20),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: (){
                              showModalBottomSheet(
                                  isDismissible: false,
                                  isScrollControlled: true,
                                  context: context,
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(30),
                                      topLeft: Radius.circular(30),
                                    ),
                                  ),
                                  builder: (context) {
                                    return Wrap(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.only(
                                                      right: 10,
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: Text(
                                                        "Cancel",
                                                        style: TextStyle(
                                                          fontSize: ScreenUtil().setSp(15),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Text(
                                                "Payment",
                                                style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(25),
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(40),
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    totalMonthly.toString(),
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(50),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                  Text(
                                                    'USD',
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(18),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(30),
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    '578',
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(20),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                  Text(
                                                    '\$LONG',
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(12),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(30),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.symmetric(
                                                  horizontal: 10,
                                                ),
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(10),
                                                        color: AppColors.accent.withOpacity(0.3),
                                                      ),
                                                      padding: const EdgeInsets.symmetric(
                                                        horizontal: 15,
                                                        vertical: 15,
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            height : ScreenUtil().setHeight(35),
                                                            width : ScreenUtil().setHeight(35),
                                                            decoration: BoxDecoration(
                                                              shape: BoxShape.circle,
                                                              color: AppColors.accent,
                                                              image: const DecorationImage(
                                                                image: AssetImage('assets/images/long.png'),
                                                                fit: BoxFit.scaleDown,
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: ScreenUtil().setWidth(10),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment : CrossAxisAlignment.start,
                                                            children: [
                                                              Text(
                                                                "\$LONG Balance",
                                                                style: TextStyle(
                                                                  fontSize: ScreenUtil().setSp(18),
                                                                  fontWeight: FontWeight.w400,
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              ),
                                                              Wrap(
                                                                children: [
                                                                  Text(
                                                                    'Balance: 3.578',
                                                                    style: TextStyle(
                                                                      fontSize: ScreenUtil().setSp(14),
                                                                      fontFamily: 'DMMono',
                                                                      color: AppColors.primary.withOpacity(0.8),
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    ' | ',
                                                                    style: TextStyle(
                                                                      fontSize: ScreenUtil().setSp(14),
                                                                      fontFamily: 'DMMono',
                                                                      color: AppColors.primary.withOpacity(0.8),
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    'After: 3.000',
                                                                    style: TextStyle(
                                                                      fontSize: ScreenUtil().setSp(14),
                                                                      fontFamily: 'DMMono',
                                                                      color: AppColors.primary.withOpacity(0.8),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: ScreenUtil().setHeight(10),
                                                    ),
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(10),
                                                        color: AppColors.primary.withOpacity(0.1),
                                                      ),
                                                      padding: const EdgeInsets.symmetric(
                                                        horizontal: 15,
                                                        vertical: 25,
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            height : ScreenUtil().setHeight(35),
                                                            width : ScreenUtil().setHeight(35),
                                                            decoration: const BoxDecoration(
                                                                shape: BoxShape.circle,
                                                                image: DecorationImage(image: AssetImage('assets/images/apple.png'),
                                                                )
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: ScreenUtil().setWidth(10),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment : CrossAxisAlignment.start,
                                                            children: [
                                                              Text(
                                                                "App Store",
                                                                style: TextStyle(
                                                                  fontSize: ScreenUtil().setSp(18),
                                                                  fontWeight: FontWeight.w400,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(30),
                                              ),
                                              InkWell(
                                                onTap: (){
                                                  Navigator.pop(context);
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(50),
                                                    color: AppColors.accent,
                                                  ),
                                                  padding: const EdgeInsets.symmetric(
                                                    horizontal: 38,
                                                    vertical: 10,
                                                  ),
                                                  child: Text(
                                                    "Pay Now",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: ScreenUtil().setSp(20),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(30),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: AppColors.accent,
                              ),
                              padding: const EdgeInsets.symmetric(
                                horizontal: 38,
                                vertical: 10,
                              ),
                              child: Text(
                                "Pay Now",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: ScreenUtil().setSp(20),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 5,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Auto renewal subscription',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        'billed every month',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                onPressed: () {
                                  if (renewalSubscription != 1) {
                                    setState(() {
                                      renewalSubscription--;
                                      totalRenewal =
                                          autoRenewalCost * renewalSubscription;
                                    });
                                  }
                                },
                                splashColor: Colors.transparent,
                                icon: const Icon(Icons.remove),
                              ),
                              Padding(
                                padding:
                                const EdgeInsets.symmetric(horizontal: 10),
                                child: Text(
                                  renewalSubscription
                                      .toString()
                                      .padLeft(2, '0'),
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(20),
                                  ),
                                ),
                              ),
                              IconButton(
                                onPressed: () {
                                  setState(() {
                                    renewalSubscription++;
                                    totalRenewal =
                                        autoRenewalCost * renewalSubscription;
                                  });
                                },
                                splashColor: Colors.transparent,
                                icon: const Icon(Icons.add),
                              ),
                            ],
                          ),
                          Text(
                            "USD\$ $totalRenewal",
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(20),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: (){
                              showModalBottomSheet(
                                  isDismissible: false,
                                  isScrollControlled: true,
                                  context: context,
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(30),
                                      topLeft: Radius.circular(30),
                                    ),
                                  ),
                                  builder: (context) {
                                    return Wrap(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.only(
                                                      right: 10,
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: Text(
                                                        "Cancel",
                                                        style: TextStyle(
                                                          fontSize: ScreenUtil().setSp(15),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Text(
                                                "Payment",
                                                style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(25),
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(40),
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    totalRenewal.toString(),
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(50),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                  Text(
                                                    'USD',
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(18),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(30),
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    '578',
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(20),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                  Text(
                                                    '\$LONG',
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(12),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(30),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.symmetric(
                                                  horizontal: 10,
                                                ),
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(10),
                                                        color: AppColors.accent.withOpacity(0.3),
                                                      ),
                                                      padding: const EdgeInsets.symmetric(
                                                        horizontal: 15,
                                                        vertical: 15,
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            height : ScreenUtil().setHeight(35),
                                                            width : ScreenUtil().setHeight(35),
                                                            decoration: BoxDecoration(
                                                              shape: BoxShape.circle,
                                                              color: AppColors.accent,
                                                              image: const DecorationImage(
                                                                image: AssetImage('assets/images/long.png'),
                                                                fit: BoxFit.scaleDown,
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: ScreenUtil().setWidth(10),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment : CrossAxisAlignment.start,
                                                            children: [
                                                              Text(
                                                                "\$LONG Balance",
                                                                style: TextStyle(
                                                                  fontSize: ScreenUtil().setSp(18),
                                                                  fontWeight: FontWeight.w400,
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              ),
                                                              Wrap(
                                                                children: [
                                                                  Text(
                                                                    'Balance: 3.578',
                                                                    style: TextStyle(
                                                                      fontSize: ScreenUtil().setSp(14),
                                                                      fontFamily: 'DMMono',
                                                                      color: AppColors.primary.withOpacity(0.8),
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    ' | ',
                                                                    style: TextStyle(
                                                                      fontSize: ScreenUtil().setSp(14),
                                                                      fontFamily: 'DMMono',
                                                                      color: AppColors.primary.withOpacity(0.8),
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    'After: 3.000',
                                                                    style: TextStyle(
                                                                      fontSize: ScreenUtil().setSp(14),
                                                                      fontFamily: 'DMMono',
                                                                      color: AppColors.primary.withOpacity(0.8),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: ScreenUtil().setHeight(10),
                                                    ),
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(10),
                                                        color: AppColors.accent.withOpacity(0.1),
                                                      ),
                                                      padding: const EdgeInsets.symmetric(
                                                        horizontal: 15,
                                                        vertical: 25,
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            height : ScreenUtil().setHeight(35),
                                                            width : ScreenUtil().setHeight(35),
                                                            decoration: const BoxDecoration(
                                                                shape: BoxShape.circle,
                                                                image: DecorationImage(image: AssetImage('assets/images/apple.png'),
                                                                )
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: ScreenUtil().setWidth(10),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment : CrossAxisAlignment.start,
                                                            children: [
                                                              Text(
                                                                "App Store",
                                                                style: TextStyle(
                                                                  fontSize: ScreenUtil().setSp(18),
                                                                  fontWeight: FontWeight.w400,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(30),
                                              ),
                                              InkWell(
                                                onTap: (){
                                                  Navigator.pop(context);
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(50),
                                                    color: AppColors.accent,
                                                  ),
                                                  padding: const EdgeInsets.symmetric(
                                                    horizontal: 38,
                                                    vertical: 10,
                                                  ),
                                                  child: Text(
                                                    "Pay Now",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: ScreenUtil().setSp(20),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: ScreenUtil().setHeight(30),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: AppColors.primary,
                              ),
                              padding: const EdgeInsets.symmetric(
                                horizontal: 38,
                                vertical: 10,
                              ),
                              child: Text(
                                "Pay Now",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: ScreenUtil().setSp(20),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
