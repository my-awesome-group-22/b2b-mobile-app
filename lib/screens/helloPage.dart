import 'package:b2b/screens/dashboardPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:line_icons/line_icons.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

import '../constants/terms_privacy.dart';
import '../styles.dart';

class HelloPage extends StatefulWidget {
  const HelloPage({Key? key}) : super(key: key);

  @override
  State<HelloPage> createState() => _HelloPageState();
}

class _HelloPageState extends State<HelloPage> {
  late ItemScrollController itemScrollController;
  late ItemPositionsListener itemPositionsListener;

  @override
  void initState() {
    itemScrollController = ItemScrollController();
    itemPositionsListener = ItemPositionsListener.create();
    super.initState();
  }

  void scrollTo(int index) {
    itemScrollController.scrollTo(
      index: index,
      duration: const Duration(milliseconds: 10),
      curve: Curves.linear,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: false,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(
            LineIcons.angleLeft,
            color: AppColors.secondary,
            size: 30,
          ),
          onPressed: () {},
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: ScreenUtil().setWidth(80),
                      child: Image.asset(
                        "assets/img/logo.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Let's get started\nwith your",
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                            color: AppColors.secondary,
                            fontWeight: FontWeight.w500,
                          ),
                    ),
                    Text(
                      "Digital BioBank",
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                            color: AppColors.accent,
                            fontWeight: FontWeight.w500,
                          ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                RichText(
                  text: TextSpan(
                      text:
                          "By using our app and creating an account you agree with our ",
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            color: AppColors.secondary,
                            fontWeight: FontWeight.w400,
                          ),
                      children: [
                        WidgetSpan(
                          child: InkWell(
                            onTap: () {
                              showModalBottomSheet(
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20),
                                  ),
                                ),
                                context: context,
                                builder: (context) {
                                  return DraggableScrollableSheet(
                                      snap: true,
                                      builder: (context, controller) {
                                        return Padding(
                                          padding: const EdgeInsets.all(20.0),
                                          child: SingleChildScrollView(
                                            controller: controller,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  "Terms & Privacy",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                                const SizedBox(
                                                  height: 20,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 20,
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      Column(
                                                        children: [
                                                          InkWell(
                                                            onTap: () {
                                                              scrollTo(0);
                                                            },
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "Terms of Service",
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .headline6!
                                                                      .copyWith(
                                                                        color: AppColors
                                                                            .accent,
                                                                      ),
                                                                ),
                                                                Icon(
                                                                  Icons
                                                                      .chevron_right,
                                                                  color: AppColors
                                                                      .primary,
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 20,
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              scrollTo(1);
                                                            },
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "Privacy Policy",
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .headline6!
                                                                      .copyWith(
                                                                        color: AppColors
                                                                            .accent,
                                                                      ),
                                                                ),
                                                                Icon(
                                                                  Icons
                                                                      .chevron_right,
                                                                  color: AppColors
                                                                      .primary,
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 20,
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              scrollTo(2);
                                                            },
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "Demo mode Terms",
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .headline6!
                                                                      .copyWith(
                                                                        color: AppColors
                                                                            .accent,
                                                                      ),
                                                                ),
                                                                Icon(
                                                                  Icons
                                                                      .chevron_right,
                                                                  color: AppColors
                                                                      .primary,
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      const SizedBox(
                                                        height: 30,
                                                      ),
                                                      ScrollablePositionedList
                                                          .builder(
                                                        shrinkWrap: true,
                                                        itemScrollController:
                                                            itemScrollController,
                                                        itemPositionsListener:
                                                            itemPositionsListener,
                                                        itemCount:
                                                            termsCondition
                                                                .length,
                                                        itemBuilder:
                                                            (context, index) {
                                                          return Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                termsCondition[
                                                                        index]
                                                                    .title,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .headline6!
                                                                    .copyWith(
                                                                      color: AppColors
                                                                          .primary,
                                                                    ),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              ),
                                                              Text(
                                                                termsCondition[
                                                                        index]
                                                                    .content,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyLarge!
                                                                    .copyWith(
                                                                      color: AppColors
                                                                          .primary,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                    ),
                                                              ),
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                            ],
                                                          );
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      });
                                },
                              );
                            },
                            child: Text(
                              'Terms of Service',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: AppColors.accent,
                                    fontWeight: FontWeight.w400,
                                  ),
                            ),
                          ),
                        ),
                        TextSpan(
                          text: ' and ',
                          style:
                              Theme.of(context).textTheme.titleMedium!.copyWith(
                                    color: AppColors.secondary,
                                    fontWeight: FontWeight.w400,
                                  ),
                        ),
                        WidgetSpan(
                          child: InkWell(
                            onTap: () {
                              showModalBottomSheet(
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20),
                                  ),
                                ),
                                context: context,
                                builder: (context) {
                                  return DraggableScrollableSheet(
                                      snap: true,
                                      builder: (context, controller) {
                                        return Padding(
                                          padding: const EdgeInsets.all(20.0),
                                          child: SingleChildScrollView(
                                            controller: controller,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  "Terms & Privacy",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5!
                                                      .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                      ),
                                                ),
                                                const SizedBox(
                                                  height: 20,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 20,
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      Column(
                                                        children: [
                                                          InkWell(
                                                            onTap: () {
                                                              scrollTo(0);
                                                            },
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "Terms of Service",
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .headline6!
                                                                      .copyWith(
                                                                        color: AppColors
                                                                            .accent,
                                                                      ),
                                                                ),
                                                                Icon(
                                                                  Icons
                                                                      .chevron_right,
                                                                  color: AppColors
                                                                      .primary,
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 20,
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              scrollTo(1);
                                                            },
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "Privacy Policy",
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .headline6!
                                                                      .copyWith(
                                                                        color: AppColors
                                                                            .accent,
                                                                      ),
                                                                ),
                                                                Icon(
                                                                  Icons
                                                                      .chevron_right,
                                                                  color: AppColors
                                                                      .primary,
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 20,
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              scrollTo(2);
                                                            },
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "Demo mode Terms",
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .headline6!
                                                                      .copyWith(
                                                                        color: AppColors
                                                                            .accent,
                                                                      ),
                                                                ),
                                                                Icon(
                                                                  Icons
                                                                      .chevron_right,
                                                                  color: AppColors
                                                                      .primary,
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      const SizedBox(
                                                        height: 30,
                                                      ),
                                                      ScrollablePositionedList
                                                          .builder(
                                                        shrinkWrap: true,
                                                        itemScrollController:
                                                            itemScrollController,
                                                        itemPositionsListener:
                                                            itemPositionsListener,
                                                        itemCount:
                                                            termsCondition
                                                                .length,
                                                        itemBuilder:
                                                            (context, index) {
                                                          return Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                termsCondition[
                                                                        index]
                                                                    .title,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .headline6!
                                                                    .copyWith(
                                                                      color: AppColors
                                                                          .primary,
                                                                    ),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              ),
                                                              Text(
                                                                termsCondition[
                                                                        index]
                                                                    .content,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyLarge!
                                                                    .copyWith(
                                                                      color: AppColors
                                                                          .primary,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                    ),
                                                              ),
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                            ],
                                                          );
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      });
                                },
                              );
                            },
                            child: Text(
                              'Privacy Policy.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: AppColors.accent,
                                    fontWeight: FontWeight.w400,
                                  ),
                            ),
                          ),
                        ),
                      ]),
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/img/earn.png",
                        ),
                        Text(
                          "50",
                          style:
                              Theme.of(context).textTheme.headline5!.copyWith(
                                    color: AppColors.accent,
                                    fontWeight: FontWeight.w500,
                                  ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: Text(
                        "Create account to receive 50 LongevityCoin (\$LONG)",
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: AppColors.primary,
                                  fontWeight: FontWeight.w100,
                                ),
                        maxLines: 2,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Icon(
                        Icons.info_outline,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => const DashboardPage(),
                      ),
                    );
                  },
                  child: Container(
                    width: ScreenUtil().screenWidth,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: AppColors.accent,
                    ),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "Here We Go",
                          style:
                              Theme.of(context).textTheme.titleLarge!.copyWith(
                                    color: Colors.white,
                                  ),
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
