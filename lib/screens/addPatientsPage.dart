import 'package:b2b/component/chatBubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:line_icons/line_icons.dart';

import '../component/customButton.dart';
import '../styles.dart';

class AddPatientPage extends StatefulWidget {
  const AddPatientPage({Key? key}) : super(key: key);

  @override
  State<AddPatientPage> createState() => _AddPatientPageState();
}

class _AddPatientPageState extends State<AddPatientPage> {
  late String sharePermissionToOrganization = "";
  String selectedSex = "";
  String selectedDate = "00 | Jan | 1970";

  List<String> permissions = [
    "Allow",
    "Allow see",
    "Deny",
  ];
  List<String> sex =[
    "male",
    "female",
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.chevron_left,
            color: AppColors.primary,
            size: Theme.of(context).textTheme.headline3!.fontSize,
          ),
        ),
        title: Text(
          "Add Patient",
          style: Theme.of(context).textTheme.headline5!.copyWith(
                color: AppColors.primary,
                fontWeight: FontWeight.w500,
              ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: 40,
        ),
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColors.accent.withOpacity(0.1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 15,
                      ),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Import, sync or dericlty ask for patient biomarkers to create his profile",
                            style:
                                Theme.of(context).textTheme.headline6!.copyWith(
                                      color: AppColors.primary,
                                      fontWeight: FontWeight.w400,
                                      fontSize: ScreenUtil().setSp(15),
                                    ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 20,
                                  vertical: 5,
                                ),
                                child: Text(
                                  "Dismiss",
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: AppColors.accent,
                                        fontWeight: FontWeight.w500,
                                      ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Import biomarkers",
                        style: Theme.of(context).textTheme.headline5!.copyWith(
                              color: AppColors.primary,
                              fontWeight: FontWeight.w500,
                            ),
                      ),
                      Text(
                        "Missing data will default to neutral values while running AI models, you will be notified to add.",
                        style: Theme.of(context).textTheme.caption!.copyWith(
                              color: Colors.red.withOpacity(0.8),
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Wrap(
                    runSpacing: 10,
                    spacing: 10,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CustomButton(
                            title: "From organization",
                            icon: LineIcons.download,
                            onPressed: () {},
                          ),
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CustomButton(
                            title: "From File",
                            icon: LineIcons.fileAlt,
                            onPressed: () {},
                          ),
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CustomButton(
                            title: "Using Disease tracker",
                            icon: LineIcons.qrcode,
                            onPressed: () {},
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 40,
                ),
                child: InkWell(
                  onTap: () {},
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: 'Download file template',
                      style: Theme.of(context).textTheme.caption!.copyWith(
                            color: AppColors.accent,
                          ),
                      children: [
                        TextSpan(
                          text: ' for easier & quicker import tapping here',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                color: AppColors.primary,
                              ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ChatBubble(
                        content: Text(
                          "Share this patient with your organization?",
                          style:
                              Theme.of(context).textTheme.titleSmall!.copyWith(
                                    color: AppColors.primary,
                                    fontWeight: FontWeight.w400,
                                  ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          const SizedBox(
                            width: 5,
                          ),
                          Icon(
                            Icons.info_outline,
                            size: Theme.of(context).textTheme.button!.fontSize,
                            color: AppColors.primary.withOpacity(0.8),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Use username or email to start a new chat.",
                            style:
                                Theme.of(context).textTheme.caption!.copyWith(
                                      color: AppColors.primary.withOpacity(0.8),
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: ScreenUtil().setHeight(25),
                        child: ListView.separated(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: permissions.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                setState(() {
                                  sharePermissionToOrganization =
                                      permissions[index];
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: sharePermissionToOrganization ==
                                          permissions[index]
                                      ? AppColors.accent
                                      : AppColors.accent.withOpacity(0.1),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 15,
                                    vertical: 5,
                                  ),
                                  child: Text(
                                    permissions[index],
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(
                                          color:
                                              sharePermissionToOrganization ==
                                                      permissions[index]
                                                  ? Colors.white
                                                  : AppColors.primary,
                                        ),
                                  ),
                                ),
                              ),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return const SizedBox(
                              width: 10,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ChatBubble(
                    content: Text(
                      "Hi\nI’m your assistant and will help you with data collection of your patient. Interact to add biomarkers manually",
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.primary,
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ChatBubble(
                    content: Text(
                      "What is their birth date?",
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.primary,
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                selectedDate,
                                style:
                                    Theme.of(context).textTheme.headline5!.copyWith(
                                          color: AppColors.accent.withOpacity(0.3),
                                        ),
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () async{
                              DateTime? selectedTime = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(1900),
                                lastDate: DateTime.now(),
                              );
                              setState(() {
                                selectedDate = DateFormat("dd | MM | yyyy").format(selectedTime!);
                              });
                            },
                            icon: Icon(
                              LineIcons.calendar,
                              color: AppColors.primary,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        "Tell Age Instead",
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: AppColors.accent,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ChatBubble(
                    content: Text(
                      "With what sex was born?",
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: AppColors.primary,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: ScreenUtil().setHeight(25),
                        child: ListView.separated(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: sex.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                setState(() {
                                  selectedSex = sex[index];
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: selectedSex == sex[index]
                                      ? AppColors.accent
                                      : AppColors.accent.withOpacity(0.1),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 15,
                                    vertical: 5,
                                  ),
                                  child: Text(
                                    sex[index],
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(
                                      color:
                                      selectedSex == sex[index]
                                          ? Colors.white
                                          : AppColors.primary,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return const SizedBox(
                              width: 10,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      bottomSheet: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: 20,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: AppColors.accentLight.withOpacity(0.2),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15,
                    vertical: 10,
                  ),
                  child: Text(
                    "Send message",
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(
                      color: AppColors.primary,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
              width: 10,
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.screen_rotation,
                color: AppColors.accent,
                size: Theme.of(context)
                    .textTheme
                    .headline5!
                    .fontSize,
              ),
            ),
            const SizedBox(
              height: 10,
              width: 10,
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.send,
                color: AppColors.accent,
                size: Theme.of(context)
                    .textTheme
                    .headline5!
                    .fontSize,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
