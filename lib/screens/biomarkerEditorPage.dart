import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../styles.dart';

class BiomarkerEditorPage extends StatefulWidget {
  const BiomarkerEditorPage({Key? key}) : super(key: key);

  @override
  State<BiomarkerEditorPage> createState() => _BiomarkerEditorPageState();
}

class _BiomarkerEditorPageState extends State<BiomarkerEditorPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.chevron_left,
            color: AppColors.primary,
            size: Theme.of(context).textTheme.headline3!.fontSize,
          ),
        ),
        title: Text(
          "Biomarkers editor",
          style: Theme.of(context).textTheme.headline5!.copyWith(
            color: AppColors.primary,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.yellow.withOpacity(0.2),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "View only mode",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                          color: AppColors.primary,
                          fontSize: ScreenUtil().setSp(18),
                        ),
                      ),
                      Text(
                        "Doctor of this patient allowed only read of biomarkers",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                          color: AppColors.primary,
                          fontWeight: FontWeight.w400,
                          fontSize: ScreenUtil().setSp(18),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () {},
                            child: Text(
                              "Request to Edit",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                color: AppColors.accent,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {},
                      borderRadius: BorderRadius.circular(80),
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppColors.accentLight.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(80),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 15,
                            vertical: 10,
                          ),
                          child: Text(
                            "Edit mode",
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                              color: AppColors.primary,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    InkWell(
                      onTap: () {
                      },
                      borderRadius: BorderRadius.circular(80),
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppColors.accentLight,
                          borderRadius: BorderRadius.circular(80),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 15,
                            vertical: 10,
                          ),
                          child: Text(
                            "On",
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: DataTable(
                  headingRowColor:
                  MaterialStateColor.resolveWith(
                        (states) => Colors.green.shade800.withOpacity(0.2),
                  ),
                  columnSpacing : 20,
                  horizontalMargin : 10,
                  border : TableBorder.all(
                    width: 5.0,
                    color: Colors.white,
                  ),
                  columns: [
                    DataColumn(
                      label: Text(
                        "Biomarkers",
                        style: Theme.of(context)
                            .textTheme
                            .subtitle1!
                            .copyWith(
                          color: AppColors.primary,
                        ),
                      ),
                    ),
                    DataColumn(
                      label: Text(
                        "old value",
                        style: Theme.of(context)
                            .textTheme
                            .subtitle1!
                            .copyWith(
                          color: AppColors.primary,
                        ),
                      ),
                    ),
                    DataColumn(
                      label: Text(
                        "new value",
                        style: Theme.of(context)
                            .textTheme
                            .subtitle1!
                            .copyWith(
                          color: AppColors.primary,
                        ),
                      ),
                    ),
                    DataColumn(
                      label: Text(
                        "-",
                        style: Theme.of(context)
                            .textTheme
                            .subtitle1!
                            .copyWith(
                          color: AppColors.primary,
                        ),
                      ),
                    ),
                    DataColumn(
                      label: Icon(
                        Icons.delete_outline,
                        color: Colors.redAccent,
                        size: Theme.of(context)
                            .textTheme
                            .headline5!
                            .fontSize,
                      ),
                    ),
                    DataColumn(
                      label: Icon(
                        Icons.info_outline,
                        color: AppColors.primary,
                        size: Theme.of(context)
                            .textTheme
                            .headline5!
                            .fontSize,
                      ),
                    ),
                  ],
                  rows: [
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood type",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "A+",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        AppColors.accentLight.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "Blood RhD",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "positive",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "-",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_outline,
                            color: Colors.redAccent,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      color: MaterialStateProperty.all(
                        Colors.red.withOpacity(0.2),
                      ),
                      cells: [
                        DataCell(
                          Text(
                            "parameter n..me",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "value",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "unit",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                        ),
                        DataCell(
                          Text(
                            "unit",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                              color:
                              AppColors.primary,
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.delete_forever_outlined,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                        DataCell(
                          Icon(
                            Icons.info_outline,
                            color: AppColors.primary,
                            size: Theme.of(context)
                                .textTheme
                                .headline5!
                                .fontSize,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
