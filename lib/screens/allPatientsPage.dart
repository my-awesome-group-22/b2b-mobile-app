import 'package:b2b/models/patientsModel.dart';
import 'package:b2b/screens/addPatientsPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:line_icons/line_icons.dart';

import '../component/customButton.dart';
import '../models/chatsModel.dart';
import '../styles.dart';

class AllPatientsPage extends StatefulWidget {
  const AllPatientsPage({Key? key}) : super(key: key);

  @override
  State<AllPatientsPage> createState() => _AllPatientsPageState();
}

class _AllPatientsPageState extends State<AllPatientsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.chevron_left,
            color: AppColors.primary,
            size: Theme.of(context).textTheme.headline3!.fontSize,
          ),
        ),
        title: Text(
          "Patients",
          style: Theme.of(context).textTheme.headline5!.copyWith(
                color: AppColors.primary,
                fontWeight: FontWeight.w500,
              ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: 40,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppColors.accent.withOpacity(0.1),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 15,
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Add, look up, update and run AI models for your patients, which makes easier to track appointments and treatment process",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: AppColors.primary,
                              fontWeight: FontWeight.w400,
                              fontSize: ScreenUtil().setSp(15),
                            ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 20,
                              vertical: 5,
                            ),
                            child: Text(
                              "Dismiss",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: AppColors.accent,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextField(
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: AppColors.primary.withOpacity(0.8),
                    ),
                decoration: InputDecoration(
                  fillColor: AppColors.accent.withOpacity(0.1),
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: AppColors.accent.withOpacity(0.1),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: AppColors.accent.withOpacity(0.1),
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red.withOpacity(0.1),
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red.withOpacity(0.1),
                    ),
                  ),
                  hintText: "Username, Name, Date...",
                  hintStyle: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: AppColors.primary.withOpacity(0.6),
                      ),
                  suffixIcon: IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.search,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    CustomButton(
                      title: "Archive",
                      icon: LineIcons.calendar,
                      onPressed: () {},
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    CustomButton(
                      title: "New Patient",
                      icon: Icons.person_add,
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => const AddPatientPage(),
                          ),
                        );
                      },
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    CustomButton(
                      title: "Quick import",
                      icon: LineIcons.upload,
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Last patient",
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      color: AppColors.primary,
                      fontWeight: FontWeight.w500,
                    ),
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: 1,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: Image(
                      image: AssetImage(
                        patients.first.profilePic,
                      ),
                    ),
                    title: Text(
                      patients.first.patientName,
                      style: Theme.of(context).textTheme.headline6!.copyWith(
                            color: AppColors.primary,
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                    subtitle: Text(
                      patients.first.patientLastAppointment,
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            color: AppColors.primary,
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                  );
                },
              ),
              Text(
                "All patients",
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      color: AppColors.primary,
                      fontWeight: FontWeight.w500,
                    ),
              ),
              GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1.1,
                ),
                shrinkWrap: true,
                itemCount: patients.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  bool isOnline = chats[chats.indexWhere((element) =>
                          element.name == patients[index].patientName)]
                      .isOnline;
                  return Card(
                    elevation: 3,
                    shadowColor: AppColors.primary.withOpacity(0.4),
                    margin: const EdgeInsets.all(10),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Image(
                                image: AssetImage(
                                  patients[index].profilePic,
                                ),
                              ),
                              Stack(
                                children: [
                                  Icon(
                                    Icons.chat_bubble_outline,
                                    size: Theme.of(context)
                                        .textTheme
                                        .headline6!
                                        .fontSize,
                                    color: isOnline == true
                                        ? Colors.green
                                        : AppColors.primary,
                                  ),
                                  isOnline == true
                                      ? Positioned(
                                          bottom: 0,
                                          right: 0,
                                          child: Container(
                                            width: 10,
                                            height: 10,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.green,
                                            ),
                                          ),
                                        )
                                      : Container(),
                                ],
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            patients[index].patientName,
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(
                                  color: AppColors.primary,
                                  fontWeight: FontWeight.w500,
                                ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            patients[index].patientLastAppointment,
                            style:
                                Theme.of(context).textTheme.subtitle2!.copyWith(
                                      color: AppColors.primary,
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
