import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_icons/line_icons.dart';

import '../styles.dart';

class ProfilePicture extends StatefulWidget {
  const ProfilePicture({Key? key}) : super(key: key);

  @override
  State<ProfilePicture> createState() => _ProfilePictureState();
}

class _ProfilePictureState extends State<ProfilePicture> {
  double zoom = 1;
  final ImagePicker picker = ImagePicker();
  late File profilePic;
  bool profileSelected = false, profilePictureSaved = false;

  @override
  void initState() {
    super.initState();
  }

  void getImage(ImageSource source) async {
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: source,
    );
    if (pickedFile != null) {
      setState(() {
        profileSelected = true;
        profilePic = File(pickedFile.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(
            LineIcons.angleLeft,
            color: AppColors.secondary,
            size: 30,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                RichText(
                  text: TextSpan(
                    text: "Let's take a ",
                    style: TextStyle(
                      color: AppColors.secondary,
                      fontSize: 30,
                      fontWeight: FontWeight.w400,
                    ),
                    children: [
                      TextSpan(
                        text: "picture?",
                        style: TextStyle(
                          color: AppColors.accent,
                          fontSize: 30,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton.icon(
                      onPressed: () {
                        getImage(
                          ImageSource.camera,
                        );
                      },
                      icon: Icon(
                        Icons.camera_alt_outlined,
                        color: AppColors.accent,
                      ),
                      label: Text(
                        "Open camera",
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: AppColors.accent,
                                  fontWeight: FontWeight.w500,
                                ),
                      ),
                    ),
                    TextButton.icon(
                      onPressed: () {
                        getImage(
                          ImageSource.gallery,
                        );
                      },
                      icon: Icon(
                        LineIcons.image,
                        color: AppColors.accent,
                      ),
                      label: Text(
                        "Open gallery",
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: AppColors.accent,
                                  fontWeight: FontWeight.w500,
                                ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                InkWell(
                  onTap: () {
                    if (profileSelected) {
                      setState(() {
                        profilePictureSaved = true;
                      });
                    }
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Link to my avatar",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: AppColors.accentLight.withOpacity(0.4),
                            ),
                      ),
                      Icon(
                        Icons.north_east,
                        color: AppColors.accentLight.withOpacity(0.4),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  color: Colors.grey.withOpacity(0.2),
                  width: ScreenUtil().setWidth(230),
                  height: ScreenUtil().setWidth(230),
                  child: DottedBorder(
                    dashPattern: const [10, 5],
                    color: AppColors.accentLight.withOpacity(0.8),
                    borderType: BorderType.Circle,
                    child: profileSelected
                        ? Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: FileImage(
                                    profilePic,
                                  ),
                                  fit: BoxFit.cover,
                                )),
                          )
                        : Container(),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(230),
                  child: Slider(
                    value: zoom,
                    onChanged: (value) {
                      setState(() {
                        zoom = value;
                      });
                    },
                  ),
                ),
              ],
            ),
            profilePictureSaved
                ? Container(
                    decoration: BoxDecoration(
                      color: AppColors.accent,
                      borderRadius: BorderRadius.circular(40),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 40,
                        vertical: 10,
                      ),
                      child: Text(
                        "Done",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                    ),
                  )
                : Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: AppColors.accent,
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(40),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 40,
                        vertical: 10,
                      ),
                      child: Text(
                        "Back",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: AppColors.accent,
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
