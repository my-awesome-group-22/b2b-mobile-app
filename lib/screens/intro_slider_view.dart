import 'package:b2b/screens/authentication_view.dart';
import 'package:b2b/styles.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:line_icons/line_icons.dart';

class IntroSliderView extends StatefulWidget {
  const IntroSliderView({Key? key}) : super(key: key);

  @override
  State<IntroSliderView> createState() => _IntroSliderViewState();
}

class _IntroSliderViewState extends State<IntroSliderView> {
  late PageController _controller;
  late CarouselController _waveController;
  int pageIndex = 0;

  @override
  void initState() {
    _controller = PageController(
      initialPage: pageIndex,
    );
    _waveController = CarouselController();
    super.initState();
  }

  List<String> waves = [
    "assets/img/waves1.png",
    "assets/img/waves2.png",
    "assets/img/waves3.png",
    "assets/img/waves4.png",
    "assets/img/waves5.png",
  ];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
            child: PageView(
              onPageChanged: (value) {
                setState(() {
                  pageIndex = value;
                });
                _waveController.animateToPage(
                  value,
                  duration: const Duration(milliseconds: 3),
                  curve: Curves.linear,
                );
              },
              physics: const ClampingScrollPhysics(),
              controller: _controller,
              children: [
                Stack(
                  children: [
                    Positioned(
                      left: 0,
                      bottom: 30,
                      child: Image.asset(
                        "assets/img/onboardingVector1.png",
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            const SizedBox(
                              height: 50,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  "assets/img/swipe.png",
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Swipe To Continue",
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: AppColors.secondary,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        RichText(
                          text: TextSpan(
                              text: "Welcome\n",
                              style: TextStyle(
                                fontSize: 45,
                                fontWeight: FontWeight.w500,
                                color: AppColors.accent,
                              ),
                              children: [
                                TextSpan(
                                    text:
                                        "to your\nAI Health\nAssistant\nand\nBioBank",
                                    style: TextStyle(
                                      fontSize: 45,
                                      fontWeight: FontWeight.w500,
                                      color: AppColors.secondary,
                                    )),
                              ]),
                          textAlign: TextAlign.center,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                builder: (context) =>
                                const AuthenticationView(),
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Skip",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: AppColors.accent,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Icon(
                                  LineIcons.arrowRight,
                                  color: AppColors.accent,
                                  size: 25,
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: size.width - 80,
                      child: Image.asset(
                        'assets/img/image2.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 30,
                        right: 30,
                        bottom: 60,
                      ),
                      child: Text(
                        'A Digital BioBank with risk assessments and personalized recommendations that can predict severe diseases and extend your health span',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.titilliumWeb(
                          fontWeight: FontWeight.w400,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'see how it works ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.accent,
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Icon(
                            Icons.north_east,
                            size: 20,
                            color: AppColors.accent,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 150,
                      child: Image.asset(
                        'assets/img/image3.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 30,
                        right: 30,
                        bottom: 60,
                      ),
                      child: Text(
                        'Your data protected by latest encryption technology and anonymize.\nTrusted by {N} costumers & \nCertified by {Gov}, {Company1}, {CompanyN}',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.titilliumWeb(
                          fontWeight: FontWeight.w400,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'learn more ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.accent,
                              fontSize: 20,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          Icon(
                            Icons.north_east,
                            size: 20,
                            color: AppColors.accent,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 150,
                      child: Image.asset(
                        'assets/img/image4.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                    Column(
                      children: [
                        const Text(
                          'Get healthier,\nGet richer',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xff3683FC),
                            fontSize: 32,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 30,
                            right: 30,
                            bottom: 60,
                          ),
                          child: Text(
                            'Get motivated by earning crypto money for your achievements and health improvements!',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.titilliumWeb(
                              fontWeight: FontWeight.w400,
                              fontSize: 24,
                            ),
                          ),
                        ),
                      ],
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'earn more ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.accent,
                              fontSize: 20,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          Icon(
                            Icons.north_east,
                            size: 20,
                            color: AppColors.accent,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        child: Column(
                          children: [
                            const Text(
                              'What else we do',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xff3683FC),
                                fontSize: 32,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              ' We like to call it "The Longevity Ecosystem" ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 20,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ListTile(
                            leading: Image.asset(
                              "assets/img/health.png",
                            ),
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Health Network ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: AppColors.secondary,
                                    fontSize: 20,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                Icon(
                                  Icons.north_east,
                                  size: 20,
                                  color: AppColors.accent,
                                ),
                              ],
                            ),
                            subtitle: Text(
                              "Connect doctors and patients using our API to get faster diagnosis",
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 16,
                                fontStyle: FontStyle.normal,
                              ),
                              maxLines: 2,
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          ListTile(
                            leading: Image.asset(
                              "assets/img/scientific.png",
                            ),
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Scientific Resort ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: AppColors.secondary,
                                    fontSize: 20,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                Icon(
                                  Icons.north_east,
                                  size: 20,
                                  color: AppColors.accent,
                                ),
                              ],
                            ),
                            subtitle: Text(
                              "Time off with professional treatments",
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 16,
                                fontStyle: FontStyle.normal,
                              ),
                              maxLines: 2,
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          ListTile(
                            leading: Image.asset(
                              "assets/img/crypto.png",
                            ),
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Crypto ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: AppColors.secondary,
                                    fontSize: 20,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                Icon(
                                  Icons.north_east,
                                  size: 20,
                                  color: AppColors.accent,
                                ),
                              ],
                            ),
                            subtitle: Text(
                              "A token and a NFT collection of human organs",
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 16,
                                fontStyle: FontStyle.normal,
                              ),
                              maxLines: 2,
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          ListTile(
                            leading: Image.asset(
                              "assets/img/gaming.png",
                            ),
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Gaming ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: AppColors.secondary,
                                    fontSize: 20,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                Icon(
                                  Icons.north_east,
                                  size: 20,
                                  color: AppColors.accent,
                                ),
                              ],
                            ),
                            subtitle: Text(
                              "Your virtual self in from your real parameters",
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 16,
                                fontStyle: FontStyle.normal,
                              ),
                              maxLines: 2,
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          ListTile(
                            leading: Image.asset(
                              "assets/img/petition.png",
                            ),
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Petition ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: AppColors.secondary,
                                    fontSize: 20,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                Icon(
                                  Icons.north_east,
                                  size: 20,
                                  color: AppColors.accent,
                                ),
                              ],
                            ),
                            subtitle: Text(
                              "Support fundamental anti-aging researches",
                              style: TextStyle(
                                color: AppColors.secondary,
                                fontSize: 16,
                                fontStyle: FontStyle.normal,
                              ),
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Stack(
              alignment: AlignmentDirectional.bottomCenter,
              fit: StackFit.loose,
              children: [
                Positioned(
                  child: CarouselSlider(
                    carouselController: _waveController,
                    items: waves.map((e) {
                      return SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: Image.asset(
                          e,
                          fit: BoxFit.fitWidth,
                        ),
                      );
                    }).toList(),
                    options: CarouselOptions(
                      height: 120,
                      pageSnapping: true,
                      scrollPhysics: const ClampingScrollPhysics(),
                      enableInfiniteScroll: false,
                      viewportFraction: 1,
                      onPageChanged: (value, x) {
                        setState(() {
                          pageIndex = value;
                        });
                        _controller.animateToPage(
                          value,
                          duration: const Duration(milliseconds: 3),
                          curve: Curves.linear,
                        );
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 50,
                    vertical: 30,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        height: 5,
                        child: ListView.separated(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: 5,
                          itemBuilder: (context, index) {
                            return Container(
                              width: 30,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: pageIndex == index
                                    ? AppColors.secondary
                                    : AppColors.accent.withOpacity(0.3),
                              ),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return const SizedBox(
                              width: 12,
                            );
                          },
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          if (pageIndex != 4) {
                            _controller.nextPage(
                              duration: const Duration(milliseconds: 3),
                              curve: Curves.linear,
                            );
                            _waveController.nextPage(
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.linear,
                            );
                            setState(() {
                              pageIndex++;
                            });
                          } else {
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                builder: (context) =>
                                    const AuthenticationView(),
                              ),
                            );
                          }
                        },
                        child: Text(
                          (pageIndex == 4) ? "Finish" : "Next",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: AppColors.secondary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
