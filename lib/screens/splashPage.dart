import 'package:b2b/screens/intro_slider_view.dart';
import 'package:b2b/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await Future.delayed(const Duration(seconds: 5)).whenComplete((){
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (context) => const IntroSliderView(),
            ));
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: Image(
              alignment: Alignment.center,
              width: ScreenUtil().screenWidth * 0.6,
              image: const AssetImage('assets/img/logo.png'),
            ),
          ),
          Positioned(
            top: 180,
            bottom: 0,
            left: 0,
            right: 0,
            child: Center(
              child: SizedBox(
                width: 40,
                height: 40,
                child: CircularProgressIndicator(
                  color: AppColors.accent,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 50,
            left: 0,
            right: 0,
            child: Center(
              child: Text(
                'Diseases Tracker',
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: AppColors.accent,
                      fontSize: 15,
                      letterSpacing: 2.2,
                      fontWeight: FontWeight.w100,
                    ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
