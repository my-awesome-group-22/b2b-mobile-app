import 'package:b2b/component/customButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:line_icons/line_icons.dart';

import '../models/chatsModel.dart';
import '../styles.dart';

class AllMessagePage extends StatefulWidget {
  const AllMessagePage({Key? key}) : super(key: key);

  @override
  State<AllMessagePage> createState() => _AllMessagePageState();
}

class _AllMessagePageState extends State<AllMessagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.chevron_left,
            color: AppColors.primary,
            size: Theme.of(context).textTheme.headline3!.fontSize,
          ),
        ),
        title: Text(
          "Chats",
          style: Theme.of(context).textTheme.headline5!.copyWith(
                color: AppColors.primary,
                fontWeight: FontWeight.w500,
              ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: 40,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppColors.accent.withOpacity(0.1),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 15,
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "The all in one place to talk to patient and doctors and easily share results",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: AppColors.primary,
                              fontWeight: FontWeight.w400,
                              fontSize: ScreenUtil().setSp(15),
                            ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: AppColors.accent,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 20,
                                vertical: 5,
                              ),
                              child: Text(
                                "Dismiss",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                    ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextField(
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: AppColors.primary.withOpacity(0.8),
                    ),
                decoration: InputDecoration(
                  fillColor: AppColors.accent.withOpacity(0.1),
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: AppColors.accent.withOpacity(0.1),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: AppColors.accent.withOpacity(0.1),
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red.withOpacity(0.1),
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red.withOpacity(0.1),
                    ),
                  ),
                  hintText: "Username, Name, Date...",
                  hintStyle: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: AppColors.primary.withOpacity(0.6),
                      ),
                  suffixIcon: IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.search,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 5,
                  ),
                  Icon(
                    Icons.info_outline,
                    size: Theme.of(context).textTheme.button!.fontSize,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    "Use username or email to start a new chat.",
                    style: Theme.of(context).textTheme.button!.copyWith(
                          color: AppColors.primary.withOpacity(0.8),
                          fontWeight: FontWeight.w400,
                        ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  CustomButton(
                    title: "Archive",
                    icon: LineIcons.calendar,
                    onPressed: (){},
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Last patient",
                style: Theme.of(context).textTheme.headline5!.copyWith(
                  color: AppColors.primary,
                  fontWeight: FontWeight.w500,
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: 1,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: Stack(
                      children: [
                        Image(
                          image: AssetImage(
                            chats.first.profilePic,
                          ),
                        ),
                        Positioned(
                          right: 1,
                          bottom: 3,
                          child: Container(
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: chats[index].isOnline == true
                                  ? Colors.green
                                  : Colors.grey,
                            ),
                          ),
                        ),
                      ],
                    ),
                    title: Text(
                      chats.first.name,
                      style:
                      Theme.of(context).textTheme.headline6!.copyWith(
                        color: AppColors.primary,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    subtitle: Text(
                      chats.first.lastMessage,
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(
                        color: AppColors.primary,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  );
                },
              ),
              Text(
                "All patients",
                style: Theme.of(context).textTheme.headline5!.copyWith(
                  color: AppColors.primary,
                  fontWeight: FontWeight.w500,
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: chats.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: Stack(
                      children: [
                        Image(
                          image: AssetImage(
                            chats[index].profilePic,
                          ),
                        ),
                        Positioned(
                          right: 1,
                          bottom: 3,
                          child: Container(
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: chats[index].isOnline == true
                                  ? Colors.green
                                  : Colors.grey,
                            ),
                          ),
                        ),
                      ],
                    ),
                    title: Text(
                      chats[index].name,
                      style:
                      Theme.of(context).textTheme.headline6!.copyWith(
                        color: AppColors.primary,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    subtitle: Text(
                      chats[index].lastMessage,
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(
                        color: AppColors.primary,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
