import 'package:b2b/screens/authentication_view.dart';
import 'package:b2b/screens/splashPage.dart';
import 'package:b2b/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      builder: (context, child) {
        return MaterialApp(
          title: 'Longevity B2B',
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          routes: {
            '/': (context) => const SplashView(),
            '/auth': (context) => const AuthenticationView(),
          },
          theme: ThemeData(
            fontFamily: 'DMSans',
            backgroundColor: Colors.white,
            primarySwatch: Colors.blue,
            primaryColor: AppColors.secondary,
          ),
          home: child,
        );
      },
      child: const SplashView(),
    );
  }
}
